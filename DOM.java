package Day_2.Day_2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
public class DOM{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_2\\drivers\\chromedriver.exe");
        
        WebDriver driver = new ChromeDriver();
        
        driver.get("https://the-internet.herokuapp.com/large");
        
        driver.manage().window().maximize();
        
        //WebElement table = driver.findElement(By.xpath("//table//tbody//tr[3]"));
        //List<WebElement> t=table.getOptions();
        List<WebElement> t=driver.findElements(By.xpath("//div[@id='siblings']//div[@id='sibling-12.1']"));
        System.out.println(t.size());
        for(int i=0;i<t.size();i++)
        {
            System.out.println(t.get(i).getText());
            
        }
        
        driver.close();
    }
}