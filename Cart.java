package Day_2.Day_2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
public class Cart{

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_2\\drivers\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        
        driver.get("https://www.flipkart.com/apple-watch-se-gps-2nd-gen/p/itm90c4bdf00beba?pid=SMWGHWZ2ZGFZGZYG&lid=LSTSMWGHWZ2ZGFZGZYG9FY3QX&marketplace=FLIPKART&q=apple+watch&store=ajy%2Fbuh&srno=s_1_2&otracker=search&otracker1=search&fm=Search&iid=b5d49c15-8d5d-4761-8e1f-f7030fce89b7.SMWGHWZ2ZGFZGZYG.SEARCH&ppt=sp&ppn=sp&ssid=4vmltf4v9c0000001676881352398&qH=ac1abc0e63e8442c");
        
        driver.manage().window().maximize();
        
        WebElement button = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']"));
        boolean isDisplayed = button.isDisplayed();

        // performing click operation if element is displayed
        if (isDisplayed == true) {
            button.click();
        }
        
        //driver.close();
    }

}