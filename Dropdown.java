package Day_2.Day_2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.concurrent.TimeUnit;
import java.util.List;
public class Dropdown{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_2\\drivers\\chromedriver.exe");
        
        WebDriver driver = new ChromeDriver();
        
        driver.get("https://the-internet.herokuapp.com/dropdown");
        
        driver.manage().window().maximize();
        
        
        
        Select dropdown = new Select(driver.findElement(By.xpath("//*[@id='dropdown']")));
        String s="";
        List<WebElement> d=dropdown.getOptions();
        for(int i=1;i<d.size();i++)
        {
            s=String.valueOf(i);
        dropdown.selectByValue(s);
        System.out.println(d.get(i).getText()+" is selected using xpath");
        //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
        }
        Select dropdown1 = new Select(driver.findElement(By.cssSelector("#dropdown")));
        d=dropdown1.getOptions();
        for(int i=1;i<d.size();i++)
        {
            s=String.valueOf(i);
            dropdown1.selectByValue(s);
        System.out.println(d.get(i).getText()+" is selected using cssSelector");
        //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
        }
        //driver.close();
    }
}