package Day_2.Day_2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
public class Checkboxes{
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\jnikhil001\\eclipse-workspace\\Day_2\\drivers\\chromedriver.exe");
        
        WebDriver driver = new ChromeDriver();
        
        driver.get("https://the-internet.herokuapp.com/checkboxes");
        
        driver.manage().window().maximize();
        
        WebElement checkBox1 = driver.findElement(By.xpath("//*[@id=\"checkboxes\"]/input[1]"));
        boolean isDisplayed = checkBox1.isDisplayed();
        // performing click operation if element is displayed
        if (isDisplayed == true) {
            checkBox1.click();
        }
        WebElement checkBox2 = driver.findElement(By.xpath("//*[@id=\"checkboxes\"]/input[2]"));
        boolean isDisplayed2 = checkBox2.isDisplayed();
        // performing click operation if element is displayed
        if (isDisplayed2 == true) {
            checkBox2.click();
        }
        
        //driver.close();
        
    }
}